# README #

Este é um projeto com Backend e aplicação Mobile, para teste em sua empresa.


### Como está organizado o projeto? ###

* O projeto está divido em duas pastas:
* Backend - Contendo o projeto Laravel + Migration
* Mobile - 

### O que foi solicitado? ###

   **Backend**

- Os dados dessa listagem devem vir de um webservice RESTFul feito com o framework PHP Laravel 5+;

- Todos os dados devem ser salvos em um banco de dados MySQL;

- Não é necessário criar API com autenticação;

- A tabela criada deve estar na pasta 'migrations' do Laravel para facilitar a avaliação do teste;

   **Mobile**

- A listagem deve conter apenas um campo chamado "nome";

- No topo da listagem deve haver um botão "Adicionar", que ficará fixo na tela (isto é, sem mudar de posição com a rolagem da tela);

- Ao apertar o botão "Adicionar", o aplicativo deve abrir outra tela com um formulário para adicionar um registro. 

- Ao consultar a tela de listagem, o registro recém criado deve constar na lista; ** WIP **


### Anotações/Guidelines ###

* Popular a base com registros -> Exp: factory(classe, quantidade)->create()
* Acessar o console do artisan (php artisan tinker)
* factory(App\Models\Name::class, 10)->create()