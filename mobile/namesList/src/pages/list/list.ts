import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { ItemDetailsPage } from '../item-details/item-details';

import { NameService } from '../../providers/name-service';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})

export class ListPage {
  icons: string[];
  items: Array<{id: string, name: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
   public nameService: NameService
   ) {
    this.initializeItems();
  }
  
  initializeItems(){
    this.items = [];
    this.nameService.load()
    .then(data => {
      this.items = data;
    });
  }

  itemTapped(event, item) {
    this.navCtrl.push(ItemDetailsPage, {
      item: item
    });
  }

  addItem(event) {
    this.navCtrl.push(ItemDetailsPage);
  }

  getItems(ev: any){
    let val = ev.target.value;

    if (val && val.trim() != ''){
      this.items = this.items.filter((filterItem) => {
        return (filterItem.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }else{
      this.nameService.load()
      .then(data => {
        this.items = data;
      });
    }
  }



}
