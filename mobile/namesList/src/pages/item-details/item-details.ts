import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { NameService } from '../../providers/name-service';

import { ToastController } from 'ionic-angular';

import { ListPage } from '../list/list';

@Component({
  selector: 'page-item-details',
  templateUrl: 'item-details.html'
})

export class ItemDetailsPage {
  selectedItem: any;
  item: any;
  bePost: {name: null};

  constructor(public navCtrl: NavController, public navParams: NavParams, public nameService: NameService, public toastCtrl: ToastController) {
    this.selectedItem = navParams.get('item');
    
    if (typeof this.selectedItem === 'undefined'){
      this.selectedItem = {name: null};
    }
  }

  saveName() {
    this.bePost = {name: this.selectedItem.name};
    this.nameService.saveName(this.bePost)
    .then((result) => {
      this.presentToast();

      this.navCtrl.setRoot(ListPage);

    }, (err) => {
      console.log(err);
    });
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Nome adicionado com sucesso!',
      position: 'bottom',
      duration: 3000
    });
    toast.present();
  }
}
