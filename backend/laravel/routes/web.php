<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'api'], function () {

    Route::group(['prefix' => 'name'], function () {

        Route::get('', ['uses' => 'NameController@allNames']);

        Route::get('{name}', ['uses' => 'NameController@getByName']);

        Route::post('', ['uses' => 'NameController@saveName']);

        Route::put('{id}', ['uses' => 'NameController@updateName']);

        Route::delete('{id}', ['uses' => 'NameController@deleteName']);

    });
});

Route::get('/', function(){
    return 'Home';
});