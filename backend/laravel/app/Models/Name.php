<?php

namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Name extends Model{
    protected $table = 'names';
    protected $fillable = ['name'];

    public $timestamps = false;
    
    public function allNames(){
        return self::all();
    }

    public function getByName($paramName){
        $names = self::where('name', $paramName)
                 ->orWhere('name', 'like', '%'.$paramName.'%')
                 ->get();

        if (is_null($names)){
            return false;
        }

        return $names;
    }

    public function saveName(){
        $input = Input::all();
        $name = new Name();
        $name->fill($input);
        $name->save();
        return $name;
    }

    public function updateName($id){
        $input = Input::all();
        $name = self::find($id);

        if(is_null($name)){
            return false;
        }

        $name->fill($input);
        $name->save();
        return $name;
    }

    public function deleteName($id){
        $names = self::find($id);
        if(is_null($names)){
            return false;
        }
        return $names->delete();
    }

}
