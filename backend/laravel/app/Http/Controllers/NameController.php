<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests; 
use App\Http\Response; 

class NameController extends Controller{
    protected $name = null;

    public function __construct(\Name $name){
        $this->name = $name;
    }

    public function allNames(){
        return $this->name->allNames();
    }

    public function getByName($paramName){
        $responseName = $this->name->getByName($paramName);
        if(!$responseName){
            return ['response' => 'Nenhum nome foi encontrado'];
        }
        return $responseName;
    }

    public function saveName(){
        return $this->name->saveName();
    }

    public function updateName($id){
        $auxName = $this->name->updateName($id);
        if(!$auxName){
            return \Response::json(['response' => 'Nome não encontrado!'], 400);
        }
        return \Response::json(['response' => 'Nome atualizado com sucesso'], 200);
    }

    public function deleteName($id){
        $auxName = $this->name->deleteName($id);
        if(!$auxName){
            return \Response::json(['response' => 'Nome não encontrado!'], 400);
        }
        return \Response::json(['response' => 'Nome removido com sucesso'], 200);
    }
}